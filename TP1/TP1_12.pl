
/* 12) Escreva o predicado listar_arvore_identada(Elemento,Op_pi,Op_pb,Op_qtd) que permite
listar de forma indentada (tipo estrutura de diretórios) a árvore de produto do Elemento; 
Op_pi define se são apresentados os produtos intermédios (on_off); 
Op_pb define se são apresentados os produtos base (on_off); 
Op_qtd define se são apresentados as quantidades (on_off)
?- listar_arvore_identada(conjunto_transmissao,on,on,off).
conjunto_transmissao
	pedaleiro
		pedal  
		braco_pedal  
		rolamento  
		prato
	corrente
	desviador_traseiro
	desviador_dianteiro
	cassete
	mudanças_dianteira
		manete_dianteira
		bicha
-----------------------------------------------------
Notas:		
produto_base(X) -> Verificar se é um produto base
produto_intermedio(Elemento)-> Verificar se é um produto Intremedio 
*/    
:-consult('TP1_4.pl'). 
:-consult('TP1_5.pl'). 
listar_arvore_identada(E,_,_,_):-not(elemento(E)) ,
								write('nao existe elemento'),
								!.
listar_arvore_identada(E,OP_PI,OP_PB,OP_QTD):- write(E),
										nl,
										getLista(E,R),
										%write(R),
										writeLista(E,R,3,OP_PI,OP_PB,OP_QTD). 
/*Listagem de todos os campos*/
writeLista(_,[],_,_,_,_):-!.								
writeLista(E,[H|T],N,OP_PI,OP_PB,OP_QTD):- writeElement(N,E,H,OP_PI,OP_PB,OP_QTD),   %Se vier False Significa que é Base e nao tem lista
											getLista(H,R),
											N1 is N + 3,
											(writeLista(H,R,N1,OP_PI,OP_PB,OP_QTD),
											writeLista(E,T,N,OP_PI,OP_PB,OP_QTD)) .
											
getLista(E,_):- not(componente(E,_,_)),!. 
getLista(E,R):- findall(X, componente(E,X,_), R).   
 
writeElement(_,_,D,OP_PI,_,_):-produto_intermedio(D),
									OP_PI==off,
									!.
writeElement(N,E,D,OP_PI,_,OP_QTD):-produto_intermedio(D),
									OP_PI==on,
									tab(N),
									write(D),  
									writeQty(E,D,OP_QTD),
									nl,!. 
%writeElement(_,_,D,_,_,_):-produto_intermedio(D), 
%							!.
writeElement(_,_,D,_,OP_PB,_):- produto_base(D), %write('ENTOU'), 
									OP_PB==off,false. %False porque termina aqui a informação
writeElement(N,E,D,_,OP_PB,OP_QTD):- produto_base(D) ,
									OP_PB==on,
									tab(N),
									write(D),  
									writeQty(E,D,OP_QTD),
									nl,
									false. 
writeElement(_,_,_,_,_,_):-!.  
/*Quantidade*/
writeQty(_,_,OP_QTD):-OP_QTD==off,
					!.
writeQty(E,D,OP_QTD):-OP_QTD==on,  
					write(' - '),
					componente(E,D,Q),
					write(Q),!.
writeQty(_,_,_):-!. 

 





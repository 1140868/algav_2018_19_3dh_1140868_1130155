/*13)Escreva o predicado guardarBaseConhecimento(Nome) que permite guardar a base de
conhecimento num ficheiro de texto (incluindo os predicados criados dinamicamente).*/

guardarBaseConhecimento():-
    guardarBaseConhecimento('/Users/ana/algav_2018_19_3dh_1140868_1130155/TP1/bikes.txt').

guardarBaseConhecimento(Nome):-
    tell(Nome),
    listing(gerar_elemento),
    listing(produto_final),
    listing(produto_base),
    listing(produto_intermedio),
    listing(nivel),
    listing(dois_mais_profundos),
    listing(reg_custo),
    listing(calc_custo),
    told.

   
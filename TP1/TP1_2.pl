
/* 2) Escreva o predicado gerar_elemento que cria na base de conhecimento factos dinâmicos do tipo
elemento _1 que guardam os elementos que existem na árvore de produto. Assim, após executar o
predicado gerar_elemento a resposta à seguinte questão deverá ser 
*/ 
gerar_elemento():- getLista(L),insertLista(L),!. 
gerar_elemento(E):-add(E).
  
/*procura todos elementos existentes nos componentes na base de conhecimento*/ 
getLista(R):- findall(L,(componente(L,_,_);componente(_,L,_)),R ) .
 
insertLista([]):-!.
insertLista([E|T]):- (add(E),(write('Elemento = '),
							write(E),
							write(' ;')
							,nl) ),
					insertLista(T).
insertLista([_|T]):-insertLista(T).

/*Adicionar campos unicos
 add(E):-elemento(E),false. % ,write('ja existe elemento') */
add(E):-not(elemento(E)),assertz(elemento(E)).  
/*,write('Adicionado') 
--------------------------------------------------------------------------
Extra
Imprimir todos os elementos enquanto existir*/
print_elem():- elemento(E), 
					(write('Elemento = '),
						write(E),
						write(' ;')
					), 
					nl,
				false. 
				
				
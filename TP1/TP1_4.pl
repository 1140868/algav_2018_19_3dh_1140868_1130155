
/*
4) Escreva o predicado produto_base(Elemento) que permite pesquisar elementos que apenas
entram na composição de outros.
?- produto_base(X).
X = aperto_rapido ;
X = aperto_rapido_selim ;
X = aro ;
X = avanco_guiador
...
*/  
produto_base(X):- findall(X,
					(componente(_,X,_),
					not(componente(X,_,_))),
					R),
				sort(R,Result),
				member(X,Result);
				false.   
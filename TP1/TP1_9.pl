/*Escreva o predicado calc_custo(Elemento,Valor,LElemSemCusto) que permite calcular o
custo de um Elemento em função das quantidades e custos dos subprodutos que integram a
árvore do referido elemento; os subprodutos que não tenham custo definido devem ser
coleccionados na lista LElemSemCusto.*/

calculo([],0,[]).
calculo([[E1|Q1]],Valor,LElemSemCusto):-
    calc_custo(E1,V,LElemSemCusto),
    Valor is V*Q1.
calculo([[E1|Q1]|T],Valor,LElemSemCusto):-
    calc_custo(E1,V,L),
    Valor is V*Q1,
    calculo(T,V1,L1),
    Valor is V+V1,
    append(L,L1,LElemSemCusto).


calc_custo(Elemento,Valor,LElemSemCusto):-
    elemento(Elemento),
    custo(Elemento,Valor),
    LElemSemCusto=[],!.
calc_custo(Elemento,Valor,LElemSemCusto):-
    elemento(Elemento),
    findall([E1|Q1],componente(Elemento,E1,Q1),L),
    calculo(L,Valor,T),
    LElemSemCusto=[Elemento|T],!.
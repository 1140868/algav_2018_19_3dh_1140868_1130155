/*3)Escreva o predicado produto_final(Elemento) que permite pesquisar elementos que não entram
na composição de outros.*/

produto_final(X):-
    findall(X, (componente(X,_,_),\+componente(_,X,_)), R),
    sort(R,Result),
    member(X,Result);fail.

/*Resultado*/
/*
?- consult('/Users/ana/Desktop/bicicletas.txt').
true.

?- produto_final(X).

X =  bicicleta;
false.
*/


/* 10)  Escreva o predicado lista_materiais(Elemento,Qtd) que permite listar os produtos base e
respectivas quantidades e custos envolvidos na produção de uma Qtd de Elemento por ordem
decrescente de custo. A estrutura da listagem deverá ser:
Elemento : Qtd
-------------------------------------
produto_base1 , qtd1, custo1
produto_base2 , qtd2, custo2
...
*/  
:-consult('TP1_5.pl'). 
 
lista_materiais(E,_):-not(elemento(E)),write('Nao tem elemento'),!.
lista_materiais(E,_):-not(produto_intermedio(E)),write('Elemento nao tem produtos base'),!.
lista_materiais(E,Q):-(write(E),write(':'),write(Q),nl),
					write('-------------------------------------------'),nl,
					(findall([P,QF,PB],
								(componente(E,PB,Q2),
									multiplica(Q2,Q,QF),
									checkCusto(PB,QF,P) 
								) 
							,R)
					),%write(R),nl,
					sort(R,R2),
					reverse(R2,L),
					print_List(L).
checkCusto(	PB,_, 0):-not(custo(PB,_)),
					true.
checkCusto(	PB,QF, FP):-custo(PB,P1), 
					multiplica(P1,QF,FP),
					true.				
					
/*
Lista: [[P,Q,E],[P,Q,E],[P,Q,E],[P,Q,E]] 
*/
print_List([]):-!.
print_List([ [P| [Q| [E|_]]]|T]):-write(E), 
						write(','), 
						write(Q), 
						write(','),
						(P\==0,
							write(P);
							write(' (SEM VALOR) ')
						), 
						write(','),
						nl,!,	
						print_List(T).  
multiplica(P1,P2,R):- R is P1 * P2. 
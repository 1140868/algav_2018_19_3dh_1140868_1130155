
/* 8) Escreva o predicado reg_custo(Elemento,Custo) que permite criar um facto (dinâmico) com o
custo de um componente, no caso de já existir um custo para esse componente deve ser atualizado. 
Assim, a chamada do predicado reg_custo(pedal, 32) deve dar origem à criação do
facto custo(pedal, 32) ou à sua atualização.
*/
reg_custo(E,_):- not(elemento(E)),write('Nao existe elemento!'),!.
reg_custo(E,C):- not(custo(E,_)),assertz(custo(E,C)),!. /*Adicionar em ultimo*/
reg_custo(E,C):- retract(custo(E,_)) 
				,assertz(custo(E,C)).  /*Elimina e adiciona de novo*/ 


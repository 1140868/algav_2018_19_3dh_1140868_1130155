    
/*DFS - */
dfs(O,D,C):- dfs2(D,[O],C).
dfs2(D,[D|T],C):-reverse([D|T],C). % Poe o valor Destino na Lista e faz inversão dela mesma e Termina
dfs2(D,[A|T],C):- D \==A ,
				ligacao(A,X),
				\+ member(X,[A|T]),
				dfs2(D,[X,A|T],C).
				
				
				
	
/* O Primeiro nivel é sempre 0,e  caso haja 1 caminho quer dizer que é o proprio sendo 1-1 = 0  o Nivel 
Metodo Breadth-first search   
Nota: 
Variaveis:
	C- Caminho
	D- Destino (Valor inserido em EY)
	A- Valor da cabeça da Lista que está contida em L
	L- Lista (O que vai criando o caminho que já passou e verificou se é possivel)
	NC- Novo Caminho
	T- Todos os caminhos possiveis ([...],[...],[...],[...])
*/ 

bfs(O,D,C):- bfs2(D,[O],C).
bfs2(D,[[D|T]|_],C):-reverse([D|T],C). % Poe o valor Destino na Lista e faz inversão dela mesma e Termina
bfs2(D,[L|T],C):-L=[A|_], 
				findall([H|L], 
							(D\==A,  
							ligacao(A,H),
							not(member(H,L))),
						NC),  
				append(T,NC,R),  
				bfs2(D,R,C)  .  		 
/* bfs(5,[[2]],C)
componentes(2,1)
componentes(2,6)
componentes(1,4)
componentes(1,3)
componentes(1,2)
componentes(3,5)  
----------------------------------------------------------------------
D|	T							| 	L		|A|	NC					| 
---------------------------------------------------------------------
5|	[]							| [2]		|2|[[1,2],[6,2]]		| 
---------------------------------------------------------------------
5|[[1,2],[6,2]]					| [1,2]		|1|[[4,1,2],[3,1,2],[2,1,2]]|
---------------------------------------------------------------------
5|[[1,2],...,[6,2]]				| [6,2]		|6|						|
---------------------------------------------------------------------
5|[[4,1,2],[3,1,2],[2,1,2],...]	| [3,1,2]	|3|				 		|
---------------------------------------------------------------------
5|		.....					|	...		|.|		...		 		|
---------------------------------------------------------------------
5|		.....					|[5,3,1,2]	|5|				 		| C= [2,1,3,5]	
---------------------------------------------------------------------
Verificar se existe componente dos dois lados */				
ligacao(P1,P2):- componente(P1,P2,_);componente(P2,P1,_). 
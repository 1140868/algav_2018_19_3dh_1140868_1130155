/*5)Escreva o predicado produto_intermedio(Elemento) que permite pesquisar elementos que
entram na composição de outros e que são formados por outros elementos.*/
produto_intermedio(X):-
    findall(X, (componente(X,_,_),componente(_,X,_)), R),
    sort(R,Result),
    member(X,Result);false.

/*Resultado*/
/*?- consult('/Users/ana/Desktop/bicicletas.txt').
true.

?- produto_intermedio(X).
X = conjunto_selim 
X = conjunto_transmissao 
X = conjunto_travagem 
X = direccao 
X = mudancas_dianteira 
X = mudancas_traseira 
X = pedaleiro 
X = quadro 
X = roda 
X = travao_direito 
X = travao_esquerdo 
false.
*/
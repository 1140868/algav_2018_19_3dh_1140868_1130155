
/* 6) Escreva o predicado nivel(ElementoX,ElementoY,Nivel) que permite determinar a
profundidade que o ElementoY está na árvore de produto relativamente ao ElementoX,
considere que a raiz está no nível zero.
Nivel Começa com o valor 0 
*/ 
nivel(EX,EY,N):- dfs(EX,EY,C),
				write(C),
				length(C,N1), 
				N is N1-1 . 
/*
-Utilização do DFS porque é o melhor para este caso como a
 arvore está estruturada com um caminho unico.
-sendo mais rápido para a procura.


*/
 
/*7)Escreva o predicado dois_mais_profundos(ElementoRaiz,(EMax1,Prof1),(EMax2,Prof2))
que permite determinar os dois elementos, e respectivas profundidades, que estão a maior
profundidade na árvore de produto ElementoRaiz.*/

:-dynamic(profundidade/2).

auxiliar(_,[]).
auxiliar(X,[H|T]):- 
    nivel(X,H,L),
    assertz(profundidade(L,H)),
    auxiliar(X,T).

dois_mais_profundos(X,(Prof1, MaxE1),(Prof2, MaxE2)):-
    findall(Y, (componente(_,Y,_), not(componente(Y,_,_))), L),
    sort(L,R),
    auxiliar(X, R),
    findall((Z,Y), 
    profundidade(Z,Y), L1), 
    sort(L1, Res), 
    reverse(Res, Res1),
    Res1=[(Prof1, MaxE1), (Prof2, MaxE2)|_].

/*Resultado*/
/*?- reconsult('/Users/ana/Desktop/TP1_6.pl').
true.

?- reconsult('/Users/ana/Desktop/TP1_extra.pl').
true.

?- dois_mais_profundos(bicicleta,(Prof1, MaxE1),(Prof2, MaxE2)).
[bicicleta,roda,aperto_rapido]
[bicicleta,conjunto_selim,aperto_rapido_selim]
[bicicleta,roda,aro]
[bicicleta,direccao,avanco_guiador]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,pedaleiro,braco_pedal]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo]
[bicicleta,direccao,caixa_direccao]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,cassete]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,corrente]
[bicicleta,roda,cubo]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,desviador_dianteiro]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,desviador_traseiro]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,disco]
[bicicleta,quadro,escora_diagonal]
[bicicleta,quadro,escora_horizontal]
[bicicleta,conjunto_selim,espigao]
[bicicleta,quadro,forqueta_frontal]
[bicicleta,direccao,guiador]
[bicicleta,conjunto_travagem,travao_direito,manete]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,manete_dianteira]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,manete_traseira]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,pastilha]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,pedaleiro,pedal]
[bicicleta,roda,pneu]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,pedaleiro,prato]
[bicicleta,roda,raio]
[bicicleta,conjunto_travagem,travao_direito,manete,travao_esquerdo,cabo,mudancas_dianteira,bicha,mudancas_traseira,conjunto_transmissao,pedaleiro,rolamento]
[bicicleta,conjunto_selim,selim]
[bicicleta,quadro,tubo_diagonal]
[bicicleta,quadro,tubo_selim]
[bicicleta,quadro,tubo_superior]
[bicicleta,roda,valvula]
Prof1 = Prof2, Prof2 = 11,
MaxE1 = rolamento,
MaxE2 = prato .
*/

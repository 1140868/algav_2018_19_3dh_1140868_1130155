/*---------------MINIMAX---------------------------------------------------*/
playCPU(Board,PCPU,P1,Prof,NewBoard):-
						vizinho(Board,PCPU,LcolunasValidas), % Procura a Lista de pontos Disponiveis.
						LcolunasValidas\==[],%Caso esteja vazio quer dizer que terá de dar a vez a outro utilizador.
						minimax(Board,Val, P1, PCPU,PCPU, P1,Prof,NewBoard),
						write(Val),nl%Val é o valor de minimax.
						.
playCPU(Board,_,_,_,Board).

proximasPosicoes(Board,Pjogar,NBoard):-
						vizinho(Board,Pjogar,LPontosValido),%Procura todos os pontos que pode jogar
						reorderPontos(LPontosValido, LcolunasValidas), %Ordem pelo que tiver maior pontuaçao e melhor chanse de ganhar
						makeBoard(Board,LcolunasValidas,Pjogar,NBoard). %Criação de Lista de Boards(NBoard) 
utility(Board,Valor):-(
						(verificaVencedor(Board,Valor));
						Valor is 0
					).						
						
%Criação de Lista de Boards Possiveis
makeBoard(_,[],_,[]).     %auxiliar do nextBoards(), work-horse recursivo. 
makeBoard(Board,Lcol,Player,ListaBoards):-
						Lcol = [(NLinha,NCol)|T],
						makeBoard(Board,T,Player,ListaBoardsAux),  
						%write(NCol),nl,
						inserePedra(Board,Player,NCol,NLinha,NB1),
						viraPeca(NB1,NLinha, NCol, Player,B0), 
						%imprimirTabuleiro(Board),  nl, 
						%write('---------NOVO------------'),
						%write('Player'),write(Player),nl,
						%imprimirTabuleiro(B0),  nl,
						%read(N),
						append([B0],ListaBoardsAux,ListaBoards). 
 
minimax(Board,Val, _,_,_,_,0,_) :- % Maximo de profundidade
						utility(Board, Val),!.
minimax(Board, Val, Ph, Pcpu,PJogar, PAdversario,N,BestNextPos) :-  
						%write( 'MINIMAX:'), write(N),nl,
						proximasPosicoes(Board,PJogar,NextPosList),  
						NextPosList\==[],
						%write( 'NextPosList:'), write(NextPosList),nl,
						best(NextPosList, BestNextPos, Val,Ph,Pcpu,PJogar,PAdversario,N), !.
minimax(Board,Val,_,_,_,_,_,_):-
						utility(Board, Val), !.
 
best([Pos], Pos, Val,Ph,Pcpu,Pjogar,PAdversario,N) :- % There is no more position to compare
						NN is N-1,
						minimax(Pos,Val,Ph,Pcpu,PAdversario,Pjogar,NN,_), !. %trocamos de jogador.

best([Pos1 | PosList], BestPos, BestVal,Ph,Pcpu,Pjogar,PAdversario,N) :-  % There are other positions to compare
    NN is N-1,
    minimax(Pos1,Val1,Ph,Pcpu,PAdversario,Pjogar,NN,_),        %trocamos de jogador
    best(PosList, Pos2, Val2,Ph,Pcpu,Pjogar,PAdversario,N),
    betterOf(Pos1, Val1, Pos2, Val2, BestPos, BestVal,Ph,Pcpu,Pjogar,PAdversario).

% Pcpu faz MAX, Ph faz min 
betterOf(Pos0, Val0, _, Val1, Pos0, Val0,Ph,_,Pjogar,_) :-% Pos0 better than Pos1
    Ph == Pjogar, 		                      	% MIN to move in Pos0
    Val0 > Val1,!.                            	% MAX prefers the greater value

betterOf(Pos0, Val0, _, Val1, Pos0, Val0,_,Pcpu,Pjogar,_) :-   % Pos0 better than Pos1
    Pcpu == Pjogar, 			                         % MAX to move in Pos0
    Val0 < Val1, !.                            % MIN prefers the lesser value

betterOf(_, _, Pos1, Val1, Pos1, Val1,_,_,_,_). % Otherwise Pos1 better than Pos0


/*--------------------------PESO DA BOARD-------------------------------------------------*/
				
setWeightVizinhos(_,[],[]):-!.
setWeightVizinhos(Board,[(L,C)|T],NL):-
					setWeightVizinhos(Board,T,NL1),
					getWeight(Board,L,C,W),
					append([[W,(L,C)]],NL1,NL)
					. 
reorderPontos(LVizinhos,NLista):-
					boardWeight(Board),
					setWeightVizinhos(Board,LVizinhos,NLista1),
					sort(NLista1,NLista2),
					getNewList(NLista2,NLista)
					. 
getNewList([],[]):-!.
getNewList([[_,(L,C)]|T],NL):-
					getNewList(T,NL1), 
					append(NL1,[(L,C)],NL)
					.
				

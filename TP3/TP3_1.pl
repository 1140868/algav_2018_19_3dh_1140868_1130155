
/*		ESTRUTURA DA GRELHA
Descrição da grelha do jogo: Estrutura com a descrição da grelha do jogo (8 linhas por 8 colunas).
Estado inicial: Definição do estado inicial na estrutura que descreve a grelha do jogo.
		0 0 0  0  0  0 0 0
		0 0 0  0  0  0 0 0
		0 0 0  0  0  0 0 0
		0 0 0  B  X  0 0 0
		0 0 0  X  B  0 0 0
		0 0 0  0  0  0 0 0
		0 0 0  0  0  0 0 0
		0 0 0  0  0  0 0 0
	1|X - Preto
	-1|B -Branco
*/
sizeTabuleiro(8).
getProfundidade(3).
/*
Board 2 :
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,-1,1,0,0,0],
					[0,0,0,1,-1,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0]
Board 3:
		[ 0, 0, 1, 0, 1, 0, 0,0],
		[ 1, 1,-1, 1, 1, 1, 1,1],
		[-1,-1,-1,-1, 1,-1, 1,1],
		[-1,-1, 1, 1, 1, 1, 1,1],
		[-1, 1,-1, 1, 1,-1, 1,1],
		[-1, 1, 1, 1, 1,-1, 1,1],
		[-1, 1, 1, 1, 1, 1, 1,1],
		[-1, 1, 1, 1, 1, 1, 1,1]



*/
tabuleiroIni(Board):-Board = [
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,-1,1,0,0,0],
					[0,0,0,1,-1,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0],
					[0,0,0,0, 0,0,0,0]
				].  
imprimirTabuleiro(Board):-nl,write("   | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |"),nl,
						write("------------------------------------"), 
						imprimirLinha(7,Board,0),nl,
						write("------------------------------------"),
						imprimirLinha(7,Board,1),nl,
						write("------------------------------------"), 
						imprimirLinha(7,Board,2),nl,
						write("------------------------------------"), 
						imprimirLinha(7,Board,3),nl,
						write("------------------------------------"), 
						imprimirLinha(7,Board,4),nl,
						write("------------------------------------"), 
						imprimirLinha(7,Board,5),nl,
						write("------------------------------------"), 
						imprimirLinha(7,Board,6),nl,
						write("------------------------------------"), 
						imprimirLinha(7,Board,7),nl.

%nth0(?Index, ?List, ?Elem)
imprimirLinha(-1,_,Lindx):-nl,
						%Lindx1 is Lindx+1,
						write(Lindx),write(' '),!.
imprimirLinha(C,Board,Lindx):-N1 is C-1,
							imprimirLinha(N1,Board,Lindx), 
							nth0(Lindx,Board,L0),nth0(C, L0, E0),
							write(' | '),escreveletra(E0).
escreveletra(1):-write('X'),!.
escreveletra(-1):-write('O'),!.
escreveletra(_):-write(' ').

%play	 
iniciaJogo(Board,_,_,_):-printVencedor(Board).%Verifica se algum deles é vencedor
iniciaJogo(Board,P1,P2,Profundidade):-vizinho(Board,P1,L),
						L\==[],%Verificar se existe alguma possibilidade para jogar
						write('Posibilidades(L,C):'), 
						write(L),nl,
						write('Jogador X Escolha Linha:'),	
						read(NLinha),nl, 
						write('Coluna'),	
						read(NCol),
						(
							(
								number(NLinha),
								number(NCol),
								valida(Board,NLinha,NCol,P1),%Verificar se o ponto inseriu e valido
								inserePedra(Board,P1,NCol,NLinha,NB1), %Insere a pedra principal
								viraPeca(NB1,NLinha, NCol, P1,NB), %Vira as outras pecas
								%write('Computador:'),nl, 
								playCPU(NB,P2,P1,Profundidade,NBoard), %CPU a jogar ( Oponente)
								%write(NBoard) , nl,
								%write('Imprimir:'),nl, 
								imprimirTabuleiro(NBoard),
								iniciaJogo(NBoard,P1,P2,Profundidade) 
							);
							(
								write('Insira colunas e linhas validas'),nl,
								iniciaJogo(Board,P1,P2,Profundidade) 
							)
						).
iniciaJogo(Board,P1,P2,Profundidade):- write('Utilizador Sem Jogadas...'),nl,
					playCPU(Board,P2,P1,Profundidade,NBoard),
					imprimirTabuleiro(NBoard),   
					iniciaJogo(NBoard,P1,P2,Profundidade)  
					.

%Inserir a Pedra na posicao					
inserePedra(Board,Util,X1,Y1,NB):-%X1 is X-1,
						%Y1 is Y-1,
						%tabuleiroIni(Board),  
						nth0(Y1,Board,L),
						insertOnMatriz(L,Util,X1,NC),
						insertOnMatriz(Board,NC,Y1,NB)%,
						%imprimirTabuleiro(NB)
						.
%Iserir na Linha e coluna 
insertOnMatriz([_|T],E,0,[E|T]):-!.
insertOnMatriz([H|T],E,P,CR):-
						P1 is P -1,
						insertOnMatriz(T,E,P1,CR2),
						CR = [H|CR2].
/*---------------MINIMAX---------------------------------------------------*/
playCPU(Board,PCPU,P1,Prof,NewBoard):-
						vizinho(Board,PCPU,LcolunasValidas), % Procura a Lista de pontos Disponiveis.
						LcolunasValidas\==[],%Caso esteja vazio quer dizer que terá de dar a vez a outro utilizador.
						minimax(Board,Val, P1, PCPU,PCPU, P1,Prof,NewBoard),
						write(Val),nl%Val é o valor de minimax.
						.
playCPU(Board,_,_,_,Board).

proximasPosicoes(Board,Pjogar,NBoard):-
						vizinho(Board,Pjogar,LPontosValido),%Procura todos os pontos que pode jogar
						reorderPontos(LPontosValido, LcolunasValidas), %Ordem pelo que tiver maior pontuaçao e melhor chanse de ganhar
						makeBoard(Board,LcolunasValidas,Pjogar,NBoard). %Criação de Lista de Boards(NBoard) 
utility(Board,Valor):-(
						(verificaQuemEstaGanhar(Board,Valor));
						Valor is 0
					).						
						
%Criação de Lista de Boards Possiveis
makeBoard(_,[],_,[]).     %auxiliar do nextBoards(), work-horse recursivo. 
makeBoard(Board,Lcol,Player,ListaBoards):-
						Lcol = [(NLinha,NCol)|T],
						makeBoard(Board,T,Player,ListaBoardsAux),  
						%write(NCol),nl,
						inserePedra(Board,Player,NCol,NLinha,NB1),
						viraPeca(NB1,NLinha, NCol, Player,B0), 
						%imprimirTabuleiro(Board),  nl, 
						%write('---------NOVO------------'),
						%write('Player'),write(Player),nl,
						%imprimirTabuleiro(B0),  nl,
						%read(N),
						append([B0],ListaBoardsAux,ListaBoards). 
 
minimax(Board,Val, _,_,_,_,0,_) :- % Maximo de profundidade
						utility(Board, Val),!.
minimax(Board, Val, Ph, Pcpu,PJogar, PAdversario,N,BestNextPos) :-  
						%write( 'MINIMAX:'), write(N),nl,
						proximasPosicoes(Board,PJogar,NextPosList),  
						NextPosList\==[],
						%write( 'NextPosList:'), write(NextPosList),nl,
						best(NextPosList, BestNextPos, Val,Ph,Pcpu,PJogar,PAdversario,N), !.
minimax(Board,Val,_,_,_,_,_,_):-
						utility(Board, Val), !.
 
best([Pos], Pos, Val,Ph,Pcpu,Pjogar,PAdversario,N) :- % There is no more position to compare
						NN is N-1,
						minimax(Pos,Val,Ph,Pcpu,PAdversario,Pjogar,NN,_), !. %trocamos de jogador.

best([Pos1 | PosList], BestPos, BestVal,Ph,Pcpu,Pjogar,PAdversario,N) :-  % There are other positions to compare
    NN is N-1,
    minimax(Pos1,Val1,Ph,Pcpu,PAdversario,Pjogar,NN,_),        %trocamos de jogador
    best(PosList, Pos2, Val2,Ph,Pcpu,Pjogar,PAdversario,N),
    betterOf(Pos1, Val1, Pos2, Val2, BestPos, BestVal,Ph,Pcpu,Pjogar,PAdversario).

% Pcpu faz MAX, Ph faz min 
betterOf(Pos0, Val0, _, Val1, Pos0, Val0,Ph,_,Pjogar,_) :-% Pos0 better than Pos1
    Ph == Pjogar, 		                      	% MIN to move in Pos0
    Val0 > Val1,!.                            	% MAX prefers the greater value

betterOf(Pos0, Val0, _, Val1, Pos0, Val0,_,Pcpu,Pjogar,_) :-   % Pos0 better than Pos1
    Pcpu == Pjogar, 			                         % MAX to move in Pos0
    Val0 < Val1, !.                            % MIN prefers the lesser value

betterOf(_, _, Pos1, Val1, Pos1, Val1,_,_,_,_). % Otherwise Pos1 better than Pos0
						
 
/*--------------------------PESO DA BOARD-------------------------------------------------*/						
boardWeight([
				[ 4,-2, 3, 3, 3, 3,-2, 4],
				[-2,-3,-1,-1,-1,-1,-3,-2],
				[ 3,-1, 2, 1, 1, 2,-1, 3],
				[ 3,-1, 1, 0, 0, 1,-1, 3],
				[ 3,-1, 1, 0, 0, 1,-1, 3],
				[ 3,-1, 2, 1, 1, 2,-1, 3],
				[-2,-3,-1,-1,-1,-1,-3,-2],
				[ 4,-2, 3, 3, 3, 3,-2, 4]
			]).
testeWeight(L,C,W):-
				boardWeight(Board),
				getWeight(Board,L,C,W).
getWeight(Board,L,C,W):-%Peso na Tabela
				nth0(L,Board,Li),
				nth0(C,Li,W),!.
				
setWeightVizinhos(_,[],[]):-!.
setWeightVizinhos(Board,[(L,C)|T],NL):-
					setWeightVizinhos(Board,T,NL1),
					getWeight(Board,L,C,W),
					append([[W,(L,C)]],NL1,NL)
					. 
reorderPontos(LVizinhos,NLista):-
					boardWeight(Board),
					setWeightVizinhos(Board,LVizinhos,NLista1),
					sort(NLista1,NLista2),
					getNewList(NLista2,NLista)
					. 
getNewList([],[]):-!.
getNewList([[_,(L,C)]|T],NL):-
					getNewList(T,NL1), 
					append(NL1,[(L,C)],NL)
					.   
/*------------------VALIDA VENCEDOR---------------------------------------*/
printVencedor(Board):-verificaVencedor(Board,Vencedor),  
					(
						(
							Vencedor\==0, 
							write('Ganhou '), 
							escreveletra(Vencedor),
							write('!') 
						);
						(
							write('Foi empate Parabens!') 
						)
					).

verificaVencedor(Board,V):- vizinho(Board,1,H),
						H == [], 
						vizinho(Board,-1,PC),
						PC == [], 
						verificaQuemEstaGanhar(Board,V), 
					!.
verificaQuemEstaGanhar(Board,V):-
						findall(
							(L1,C1),
							(	nth0(L1,Board,Li),
								nth0(C1,Li,1) 
							),
							LH
						), 
						length(LH,NH), 
						findall(
							(L1,C1),
							(	nth0(L1,Board,Li),
								nth0(C1,Li,-1) 
							),
							LPC
						), 
						length(LPC,NPC),  
						(
							NH > NPC , V is 1; 
							(
								NPC > NH , V is -1; 
								V is 0
							)
						).
/*---------------VIRAR PECAS--------------------------------*/ 
testeViraPeca(L,C,P):-tabuleiroIni(Board),%Teste para virar Pecas
					viraPeca(Board,L,C,P,NB),
					imprimirTabuleiro(NB).

viraPeca(Board,L, C, P,FB):-
					viraDiagonal(Board,L,C,P,NB1),
					viraHorizontal(NB1,L,C,P,NB2),
					viraVertical(NB2,L,C,P,FB),
					!. 
viraDiagonal(Board, L, C, P,NB):-
					vira(Board, L, C, P,1,1,NB1),%UP Dir.
					vira(NB1, L, C, P,-1,1,NB2),%UP Esq.
					vira(NB2, L, C, P,1,-1,NB3),%Down Dir.
					vira(NB3, L, C, P,-1,-1,NB),%Down Esq.
					!.  
viraHorizontal(Board, L, C, P,NB):- 
					vira(Board, L, C, P,0,1,NB3), %Dir.
					vira(NB3, L, C, P,0,-1,NB),%Esq.
					!.
viraVertical(Board, L, C, P,NB):-  
					vira(Board, L, C, P,1,0,NB5),%UP
					vira(NB5, L, C, P,-1,0,NB),%down
					!.
vira(Board, L, C, P,ValL,ValC,NB):-% Metodo utilizado para quando está ainda na posição principal mover 1 ponto e verificar se tem o Oponente
					L1 is L + ValL,
					C1 is C + ValC,
					getElem(Board,L1,C1,Res),
					cor(P,P1),%Jogador Adversario 
					Res = P1,
					vira1(Board, L1, C1, P,P1,ValL,ValC,NB).	
vira(Board, _, _, _,_,_,Board):-!. 
/*
Board- Tabela
L- Linha
C- Coluna
P - Jogador
PO- Jogador Adversario
ValL- Incrementacao dependendo a direcao em linha
ValC - Incrementacao dependendo a direcao em coluna
NB - Nova Tabela
*/ 
vira1(Board, L, C, P,PO,ValL,ValC,NB):-
					getElem(Board,L,C,Res),
					cor(PO,P1), 
					(
						(
							Res = P1,%Caso Encontre o resultado seja igual a Jogador para e envia nova board
							NB=Board,
							!
						);
						(
							Res = PO, %Caso o resultado seja igual ao Oponente continua com incrementação e insere a nova Pedra
							inserePedra(Board,P,C,L,NB1),
							L1 is L + ValL, 
							C1 is C + ValC,
							vira1(NB1, L1, C1,P,PO,ValL,ValC,NB) 
						)
					).
			 
/*---------------VALIDACAO DA POSICAO--------------------------------*/
testeValida(L,C,P):- tabuleiroIni(Board),
					valida(Board,L,C,P).

valida(Board,L,C,P):-(validaDiagonal(Board,L,C,P);
					  validaHorizontal(Board,L,C,P);
					  validaVertical(Board,L,C,P)
					),
					!. 
/*VALIDACAO DIAGONAL*/
validaDiagonal(Board, L, C, P):- 
					valida(Board, L, C, P,1,1),!; %UP Dir.
					valida(Board, L, C, P,-1,1),!; %UP Esq.
					valida(Board, L, C, P,1,-1),!; %Down Dir.
					valida(Board, L, C, P,-1,-1),! %Down Esq.
					.  
/*VALIDACAO HORIZONTAL*/  
validaHorizontal(Board, L, C, P):- 
					valida(Board, L, C, P,1,0),!; %Horizontal Esq.
					valida(Board, L, C, P,-1,0),! %Horizontal Dir.
					.
/*VALIDACAO VERTICAL*/  
validaVertical(Board, L, C, P):- 
					valida(Board, L, C, P,0,1),!; %Vertical UP
					valida(Board, L, C, P,0,-1),! %Vertical Down
					.
valida(Board, L, C, P,ValL,ValC):- % Metodo utilizado para quando está ainda na posição principal mover 1 ponto e verificar se tem o Oponente
					L1 is L + ValL,
					C1 is C + ValC,
					getElem(Board,L1,C1,Res),
					cor(P,P1), 
					Res = P1,
					valida1(Board, L1, C1, P1,ValL,ValC).	 
valida1(Board, L, C, P,ValL,ValC):-
					getElem(Board,L,C,Res),
					cor(P,P1), 
					(
						(Res = P1,!);%Caso Encontre o resultado seja igual a Jogador para
						(
							Res = P,%Caso o resultado seja igual ao Oponente continua com incrementação
							L1 is L + ValL, 
							C1 is C + ValC,
							valida1(Board, L1, C1, P,ValL,ValC) 
						)
					).
/*---------------PRODURAR TODOS OS VIZINHOS--------------------------------*/ 
vizinho(Board,P,Lvizinhos):-findall(
							(L1,C1),
							(	nth0(L1,Board,Li),
								nth0(C1,Li,0), %Procura na matriz todos que tem posição 0
								getVizinho(Board,L1,C1,_,_,P) %Desses pontos verifica os vizinhos (Jogador Inimigo)
								,valida(Board,L1,C1,P) %Verifica se é um ponto válido
							),
							Lvizinhos),
							%write(Lvizinhos),
							!.  
%Procura a volta da linha e coluna os vizinhos qual o jogador pretendido
getVizinho(Board,L,C,VL,VC,P):-
					VC is C+1,
					VL is L+1,
					%write('Entrou Topo Direito'),nl,
					cor(P,P1),
					getElem(Board,VL,VC,P1),!.
getVizinho(Board,L,C,VL,VC,P):-
					VC is C-1,
					VL is L-1,
					%write('Entrou Baixo Esquerdo'),nl,
					cor(P,P1),
					getElem(Board,VL,VC,P1),!.
getVizinho(Board,L,C,VL,VC,P):-
					VC is C+1,
					VL is L-1,
					%write('Entrou Topo Esquerdo'),nl,
					cor(P,P1),
					getElem(Board,VL,VC,P1),!. 
getVizinho(Board,L,C,VL,VC,P):-
					VC is C-1,
					VL is L+1,
					%write('Entrou baixo Direito'),nl,
					cor(P,P1),
					getElem(Board,VL,VC,P1),!.
getVizinho(Board,L,C,L,VC,P):-
					VC is C+1,
					%write('Entrou Topo'),nl,
					cor(P,P1),
					getElem(Board,L,VC,P1),!. 
getVizinho(Board,L,C,L,VC,P):-
					VC is C-1,
					%write('Entrou Baixo'),nl,
					cor(P,P1),
					getElem(Board,L,VC,P1),!.					
getVizinho(Board,L,C,VL,C,P):-
					VL is L+1,
					%write('Entrou Direito'),nl,
					cor(P,P1),
					getElem(Board,VL,C,P1),!.
getVizinho(Board,L,C,VL,C,P):-
					VL is L-1,
					%write('Entrou Esquerdo'),nl,
					cor(P,P1),
					getElem(Board,VL,C,P1),!.					
/*-------------------------------------------------------*/ 
getElem(Board,L,C,R):- %Procura o Elemento na Board
			inBoard(L,C),
			nth0(L,Board,Li),nth0(C,Li,R).
%Verificar se está dentro do tabuleiro.				
inBoard(L,C):-L > -1 , 
			C > -1 ,
			sizeTabuleiro(S),
			C < S,
			L < S,
			!. 

%cor(CorX,CorY)	 - dar a cor do Jogador contrário 				
cor(1,-1):-!.		
cor(-1,1):-!.
/*-------------------*/ 
%Start the game.
play:-tabuleiroIni(Board),
	getProfundidade(Profundidade),
	imprimirTabuleiro(Board),nl,
    write('===================='),nl,
    write('=     Reversi      ='),nl,
    write('===================='),nl,nl,
    write(''),nl,
	iniciaJogo(Board,1,-1,Profundidade). 
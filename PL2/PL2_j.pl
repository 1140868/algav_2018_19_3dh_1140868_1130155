substituir(_,_,[],[]).
substituir(X,Y,[X|L],[Y|T]):-
    substituir(X,Y,L,T).
substituir(X,Y,[Z|L],[Z|T]):-
    X\==Z,
    substituir(X,Y,L,T).
/* m) União de dois conjuntos representados por listas (os conjuntos não
admitem elementos repetidos)
*/  
uniao([],[],[]):-fail.
uniao(L1,[],L1):-!.
uniao(L1,[H|T],R):- append(L1,[H],R1), uniao(R1,T,R).



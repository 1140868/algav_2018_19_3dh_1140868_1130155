intersecao([],_,[]).
intersecao([H|T],L,[H|R]):-
    member(H,L),
    intersecao(T,L,R).
intersecao([_|T],L,R):-
    intersecao(T,L,R).


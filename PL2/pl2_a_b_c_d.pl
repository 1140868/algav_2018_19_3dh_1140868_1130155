
/* 1.Implemente os predicados para realizar as seguintes tarefas:
 a) Obter a média de uma lista de inteiros */
 
media([],_):- write('Erro deve ter lista'),
			!,fail. 
media(L,R ):- somaContaLista(L,S1,C1), R  is S1 / C1 .
 
somaContaLista([],0,0):-!.
somaContaLista([E|L],S,C):- somaContaLista(L,S1,C1),
					S is E + S1, 
					C is C1 + 1 .
/* b) Obter o menor valor de uma lista de inteiros */ 
menor([L],L):-!.
menor([V|L],R):- menor(L,R),
			R < V.
menor([V|_],V).
/* 
	V		L			R
----------------------------------
|	42	|23,14,65	|R:14 	14<42| True
----------------------------------
|	23	|14,65		|R:14  14<23 | True
----------------------------------
|	14	|65	 		|R:65  65<14 | False entao faz menor([V|_],V).
----------------------------------
|	65	|	     	|  65        |menor([L],L).
----------------------------------  
?- menor([42,23,14,65],R).
R = 14 ;
R = 23 ;
R = 42. 
Codigo do Professor:  */
minimo([H|T],MIN):-minimo(T,H,MIN).
minimo([],M,M):-!.
minimo([H|T],M,MIN):-H<M,
					!/*Para nao dar mais valores menores*/,
					minimo(T,H,MIN).
minimo([_|T],M,MIN):-minimo(T,M,MIN).
/*2º Opção
min2([H],H).
min2([H|T],MIN):-min2(T,MT),
				(H<MT, 
					MIN=H,
					!;
					MIN =MT).

*/
/* c) Contar o número de elementos pares e ímpares numa lista de inteiros */ 
conta([],0,0):-write('Erro:Sem Elementos para Contar').
conta(L,RP,RI):- contaPares(L, RP2),RP is RP2 ,
				(contaImpares(L,RI2), RI is RI2 ). 
contaPares([],0):-!. 
contaPares([E|L],R):-contaPares(L,R1),
						(0 is E mod 2,
						R is R1 + 1; R is R1).
contaImpares([],0).
contaImpares([E|L],R):- contaImpares(L,R1), 
						(0 is E mod 2,
						R is R1 ; R is R1+1).					
/* d) Verificar se uma lista tem elementos repetidos*/ 
elemRepete(L):-\+semRepetidos(L). 
semRepetidos([]):-true.
semRepetidos([H|T]):- semRepetidos(T),\+member(H,T).%Utilização do "member" para verificar se existe
/* 
?- elemRepete([]).
false.

?- elemRepete([1,1]).
true.

?- elemRepete([1,21]).
false.

?- elemRepete([1,21,2,3,4,2]).
true.
*/
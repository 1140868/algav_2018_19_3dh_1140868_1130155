/*
o) Diferença entre dois conjuntos representados por listas, ou seja, gera um
conjunto com os elementos que pertencem a um dos dois conjuntos,
mas não a ambos*/
 
diferenca([],_,[]):-!.
diferenca(L1,[],L1):-!.
diferenca(L1,L2,R):- findall(
						L,
						(
							member(L,L1),
							not(member(L,L2))
						),
						R).
/* 
?- diferenca([1,2,3,4,5],[2,1],R).
R = [3, 4, 5].
 
?- diferenca([1,2,3,4,5,6,7,8,9,10],[2,1,4,5,8],R).
R = [3, 6, 7, 9, 10].
*/
   


/*i) Eliminar todas as ocorrências de um elemento numa lista
*/   
removeElem(_,[],[]):-!. %Lista Vazia
removeElem(E,[E|L],M):- removeElem(E,L,M),!.
removeElem(E,[Y|L],[Y|M]):- removeElem(E,L,M).

/*?- removeElem(2,[3,4,1,2,3],L).
L = [3, 4, 1, 3].

?- removeElem(3,[3,4,1,2,3],L).
L = [4, 1, 2].*/
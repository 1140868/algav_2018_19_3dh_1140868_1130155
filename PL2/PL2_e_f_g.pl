/*1-e*/
/*frente([L],[L])*/
/*apaga(X,[X|T],LR):-
    apaga(X,T,LR).
apaga(X,[H|T],[H|LR]):-
    X\=H,  
    apaga(X,T,LR). 
*/

apagar(X, [X|T], T).
apagar(X, [H|T],[H|L]):- apagar(X,T,L).


concat(L1,L2,L3):- append(L1,L2,L3).

frente(L,L2):-
    menor(L,Min), 
    apagar(Min,L,L1),
    concat([min],L1,L2),!,true.

/*1-f*/
concatenar([ ],L,L).
concatenar([X|L1],L2,[X|L3]) :-
concatenar(L1,L2,L3).

/*1-g*/

linear([],[]).
linear([H|T],L):- !, linear(H,L1), linear(T,L2), append(L1,L2,L).
linear(L,[L]):- write(L),nl.
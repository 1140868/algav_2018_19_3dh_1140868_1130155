/*Pretende-se que criem uma Base de Conhecimento (BC) com
informação relativa aos cinco continentes (africa, america, asia, europa,
oceania); alguns países com o nome do país, continente e respetiva
população (número de habitantes em milhões) e fronteiras entre os
vários países. Exemplos de factos da BC:*/
continente(europa).
continente(asia).
continente(oceania).
continente(america).
continente(africa).
/*Pais,Continente, População*/
pais(portugal,europa,30).
pais(espanha,europa,10).
pais(franca,europa,200).
pais(japao,asia,300).
pais(china,asia,300).
pais(rusia,asia,300).
pais(usa,america,500).
pais(mexico,america,500).

/*fronteira*/
fronteira(portugal,espanha).
fronteira(espanha,franca).
fronteira(rusia,china).
fronteira(mexico,usa).

/* 2. Questione através da consola do PROLOG esses mesmos factos de
diversas maneiras, com questões que permitam dar a resposta “yes” ou
"no" e questões que envolvam valores de variáveis. Exemplo:
− França faz fronteira com Espanha?
?- fronteiras(franca,espanha).
true.

?- fronteiras(espanha,franca).
false.

− Qual a população do México em milhões?
?- pais(mexico,X,Y).
X = america,
Y = 500.

 
3. Escreva os seguintes predicados:
a) vizinho(P1,P2) que sucede se os países fazem fronteira.*/ 
vizinho(P1,P2):- fronteira(P1,P2).
vizinho(P1,P2):- fronteira(P2,P1).

/*b) contSemPaises(C) que encontra os continentes sem países
representados na BC.*/
contSemPaises(C):- continente(C),\+pais(_,C,_).
/*c) semVizinhos(L) que encontra os países sem fronteiras definidas na BC.*/
semVizinhos(L) :- pais(L,_,_),( \+ fronteira(L,_), \+ fronteira(_,L)).
/*d) chegoLaFacil(P1, P2) que sucede se é possível chegar de P1 a P2,
diretamente ou atravessando unicamente um outro país.*/
chegoLaFacil(P1, P2) :- vizinho(P1, P2);( fronteira(P1,_), fronteira(_,P2)).
	












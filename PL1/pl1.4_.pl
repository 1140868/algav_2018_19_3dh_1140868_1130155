/*4. Elabore predicados recursivos para:
a) Cálculo da potência inteira (negativa e não negativa) de um número.
PotenciaV2(Base,Exp,Result)
?- potenciaV2(2,9,R).
R = 512 ;
false.

?- potenciaV2(2,-9,R).
R = 0.001953125 ;
false. 
*/ 
potenciaV2(B,E,R) :- E<0,
				expoenteNegativo(B,E,R).
potenciaV2(B,E,R) :- E>=0,
				potencia(B,E,R). 
/*Caso o Expoente seja 0, retorna 1.*/
potencia(_,0,1):- !. /*Para mandar a True e finalizar*/
potencia(B,E,R) :- E>0,/*Recrusividade em que faz do Expoente   o resultado faz-se base*valor */
				E1 is E-1, 
				potencia(B,E1,R1), R is B * R1.
/*Recrusividade em que faz no expoente(Negativo) Convertemos em positivo e utiliza a formula de cima*/
expoenteNegativo(B,E,R) :- E1 is E * -1,
						potencia(B,E1,R1), 
						R is 1/R1.
						
/*b) Cálculo do fatorial de um número.*/
fatorial(0,1) :- !. /*Para mandar a True e finalizar*/
fatorial(B,F):- B>0, 
			B1 is B-1,
			fatorial(B1,F1), 
			F is B * F1.
/*c) Calcular o somatório dos valores compreendidos entre J e K inclusive.*/ 
somatorio(J,K,R) :- J=K, 
					R=J,
					!.
somatorio(J,K,R) :- K<J,
				somatorio(K, J, R ),
				!. 
somatorio(J,K,R) :- J<K,
					J1 is J+1,
					somatorio(J1, K, R1),
					R is J+R1.  
/*d) Divisão inteira de dois números e respetivo resto.
N1-Dividendo
N2-Divisor*/
div(N1, N1, 1, 0):- !. /*Caso os dois numeros sejam iguais, retorna 1 e com resto 0*/
div(N1, N2, 0, Resto):- N1<N2, /*Resto da divisão é quando o valor do dividendo não dá mais para dividir */
					!, 
					Resto is N1.
div(N1, N2, Res, Resto):- N1>N2, /*Resultado- Soma sempre 1 ao resultado sempre que o valor for superior*/ 
					X1 is N1-N2, 
					div(X1, N2, ResAux, Resto), 
					Res is 1+ResAux. 
/* Exemplo:
-------------------------
|	8	|	2	| Resul	|
-------------------------
|	6	|	2	|	1	|
-------------------------
|	4	|	2	|	2	|
-------------------------
|	2	|	2	|	3	|
-------------------------
|	0	|	2	|	4	|
-------------------------
Resto: 0
e) Determinar se um número N é primo.*/ 
primo(2).
primo(X) :- X < 2,
			!,fail. /*Fail -> False*/
primo(X) :- X > 2,
			not(divisivel(X,2)),!.  
			
divisivel(X,Y) :- 0 is X mod Y,!. /*Se o resto da divisão der 0 então não é primo*/
divisivel(X,Y) :- X > Y+1,divisivel(X, Y+1).
/*Detalhe : devia verificar pelo 3 em vez do 2 para melhor perfomance  


f) Calcular o máximo divisor comum (m.d.c) de dois números A e B.*/ 
mdc(A,A,A):- !.
mdc(A,B,D):- A < B, B1 is B - A, mdc(A,B1,D),!.
mdc(A,B,D):- A > B, A1 is A - B, mdc(A1,B,D),!.
/* 
	A		B			D
-----------------------------
|	15	|	20	| Resul		|
-----------------------------
|	15	|	5	|A<B 	 D  |
-----------------------------
|	10	|	5	|A>B	 D	|
-----------------------------
|	5	|	5	|A>B	 D  |
-----------------------------
|	5	|	5	|	 	 5	|
-----------------------------
?- mdc(2,20,D).
D = 2.

?- mdc(15,20,D).
D = 5.

?- mdc(5,20,D).
D = 5.
  
?- mdc(24,20,D).
D = 4.
*/





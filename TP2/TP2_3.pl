
/*
Iteracão 3 
4. Implemente um predicado tsp3, que com base na solução encontrada	na questão 3,	
implemente uma heurística de melhoria da solução que tem por base o principio 
da remoção de cruzamentos.
*/ 
:-consult('TP2_2.pl').
:-consult('intersept.pl').
 
tsp3(O,NCAM):- tsp2(O,CAM,D),  %TP2_2.pl
	write("TSP2:"),write(D),tab(3), 
    write("Caminho: "),write(CAM),nl, nl,  
	newCaminho(CAM, NCAM),
	calDist(NCAM,D3),
	write("TSP3:"),write(D3),tab(3),
    write("Caminho: "),write(NCAM),nl, nl, !.

/*Retira da primeira posicao a Origem e depois faz o calculo para cada um até a lista terminar*/
calDist([O|D],C):-calDist(O,D,C),!. 
calDist(_,[],0):-!.
calDist(O,[D|L],C):-calDist(D,L,CP),
				dist_cities(O,D,CX),
				C is CP + CX.
				
/*Dá o caminho otomizado*/  
newCaminho(CAM,OTCAM):-getValidIntersect(CAM,LCRUZ), 
					%write("cruzamento: "),  write(LCRUZ), nl, %length(LCRUZ,N), write("N cruzamento: "), write(N), nl,
					( 	
						( 	LCRUZ ==[], /*Terminou*/
							OTCAM = CAM
						); 
						(   LCRUZ= [[_,A]-[B,_]|_], 
							swap(CAM,A,B,NCAM), %Correção do Cruzamento em que faz a inversão do destino A e B 
							%write("CAM: "), write(CAM),nl, write("NCAM: "), write(NCAM),nl,
							newCaminho(NCAM,OTCAMF),
							OTCAM = OTCAMF
						)
					). 
/*
Parte de um destino ao outro fazendo que essa parte da lista seja ao contrário
CAM- Lista de Entrada
A|B -(_,A)-(B,_)
LP1 - Primeira parte da Lista
LP2 - Segunda parte da lista (Contem o valor de A)
LAB - Contem a lista que começa desde o A até o B (Primeira parte da Lista)
LF - Segunda parte da lista que restou 
*/
swap(CAM,A,B,NCAM):-splitLista(0,CAM,A,LP1,LP2 ),   
				splitLista(1,LP2,B,LAB,LF),  
				reverse(LAB,LBA), %Poe o destino entre A e B ao contrário a lista
				append(LBA,LF,LBAF),
				append(LP1,LBAF,NCAM).
/*
Ex: L = [a,b,c,d,e,f,g,h], A=c, B =f 
-> splitLista(0,L,A,LP1,LP2 )  
LP1 = [a,b]
LP2 = [c,d,e,f,g,h]
->splitLista(1,LP2,B,LAB,LF)
LAB = [c,d,e,f]
LF = [g,h]
->reverse(LAB,LBA)
LBA= [f,e,d,c]
LBAF = [f,e,d,c,g,h]
NL=[a,b,f,e,d,c,g,h]
I - > Include
*/
splitLista(0,[A|T],A,[],[A|T]):-!.
splitLista(1,[B|T],B,[B],T):-!.
splitLista(I,[H|T],P,LP,LR):-splitLista(I,T,P,LP2,LR),
							append([H],LP2,LP).   
 

/*
LCRUZ- Os Cruzamentos existentes
LCAM - Lista 
C- Cruzamentos válidos
*/ 
getValidIntersect([_|[]],[]). % LCRUZ=[[A,B]-[X,Z],[A,B]-[X,Z]]  
getValidIntersect([A|[B|LCAM]],LCRUZ):-%write('ENTROU'),write(A),write('-'),write(B),nl,
									findCruz(A,B,[A|[B|LCAM]],LC), 
									%write('ENTROU findCruz'),nl,
									getValidIntersect([B|LCAM],LC2), /*B deve estar na lista porque também origem de algum destino*/
									append(LC,LC2,LCRUZ).

									
/* findCruz(Caminho,Lista Verificar, Cruzamentos).
ex: findCruz([a,b],[a,b,c,d,e,g,h],C). 
*/
findCruz(_,_,[_|[]],[]):-!. 
/*
A-Origem
B-Destino
X-Origem (proxima Ligação)
Z- Destino (Proxima Ligação)
L - Lista de pontos a visitar
C - Cruzamentos
*/
findCruz(A,B,[X|L],C):-L =[Z|_],
					findCruz(A,B,L,C2),  
					%write(A),write(','),write(B),write('-'),
					%write(X),write(','),write(Z),tab(5),
					(	( 	
							city(A,AX,AY),%city(name,latitude,longitude)
							city(B,BX,BY),
							city(X,XX,XY),
							city(Z,ZX,ZY),
							doIntersect((AX,AY),(BX,BY),(XX,XY),(ZX,ZY)), %intersept.pl 
							validCruz([A,B]-[X,Z],CVALIDO),
							%write(CVALIDO),nl,
							append(CVALIDO,C2,C)
						);
						C = C2
					).
/*Validação das cruzes
Tudo o que tenha Origem(A) igual ao Destino(Z) e Destino(B) igual à origem(X) não é valido e deve ir vazio
*/
validCruz([A,B]-[X,Z],[]):-(A==Z;
							B==X), !.
validCruz([A,B]-[X,Z],[[A,B]-[X,Z]]).

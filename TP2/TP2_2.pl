
/*
Iteracão 2 
3. Implemente o	 predicado tsp2, utilizando	 uma heurística	 greedy, a heurística do	
vizinho	mais próximo (ideia	base: próxima cidade a ser visitada é a mais próxima que	
ainda não foi visitada).
*/ 
:-consult('algav_tp2_tsp.pl').    
%Verificar que a cidade de origem existe.
tsp2(O,_,_):-not(city(O,_,_)),
			write('Nao tem cidade'),
			!.  
/*
O- Origem
CAM - Caminho
C-Custo
*/
tsp2(O,CAM,C):-findall(D,city(D,_,_),LC), 
				select(O,LC,LR), %Remove da lista a Origem ( podia-se ter utilizado no findall O\==C)
				tsp2(O,[O],LR,CAM,C),!. 
				
tsp2(O,[D|T],[],CAM,C):-dist_cities(O,D,C),
						reverse([O|[D|T]],CAM),!. 
						
tsp2(O,[D|T],LV,CAM,C):- %write('Dados:'), write([D|T]),nl, write(LV),nl, write(D),nl,
					findall((DIS,C1),
						(
							edge(D,C1,DIS),
							not(member(C1,[D|T]))
						),
					L),
					%write('Lista'), write(L),nl, write('----------------'),nl,
					sort(L,[(CX,ND)|_]), %Ordenar a lista e receber o primeiro dessa mesma e ter info. Custo(CX) e Novo Destino(ND)
					delete(LV,ND,LF), %Elimina da Lista (LV) o novo destino (ND).
					tsp2(O,[ND,D|T],LF,CAM,CP), 
					C is CP + CX. %Soma o custo da distancia. 
/*
	O-Origem
	D-Distancia
	C-Cidade
*/
edge(O,C,D):-city(C,_,_),
			O\==C,
			dist_cities(O,C,D). 
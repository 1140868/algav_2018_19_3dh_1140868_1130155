
/*Iteracao 4
5. Implemente o	predicado tsp4, recorrendo a algoritmos genéticos
The basic process for a genetic algorithm is:
    -Initialization - Create an initial population. This population is usually randomly generated and can be any desired size, from only 
    a few individuals to thousands.
    -Evaluation - Each member of the population is then evaluated and we calculate a 'fitness' for that individual. The fitness value is 
    calculated by how well it fits with our desired requirements. These requirements could be simple, 'faster algorithms are better', or 
    more complex, 'stronger materials are better but they shouldn't be too heavy'.
    -Selection - We want to be constantly improving our populations overall fitness. Selection helps us to do this by discarding the bad 
    designs and only keeping the best individuals in the population.  There are a few different selection methods but the basic idea is 
    the same, make it more likely that fitter individuals will be selected for our next generation.
    -Crossover - During crossover we create new individuals by combining aspects of our selected individuals. We can think of this as 
    mimicking how sex works in nature. The hope is that by combining certain traits from two or more individuals we will create an even 
    'fitter' offspring which will inherit the best traits from each of it's parents.
    -Mutation - We need to add a little bit randomness into our populations' genetics otherwise every combination of solutions we can 
    create would be in our initial population. Mutation typically works by making very small changes at random to an individuals genome.
    -And repeat! - Now we have our next generation we can start again from step two until we reach a termination condition.

*/ 
:-consult('algav_tp2_tsp.pl').
:-consult('TP2_3.pl'). 
%parameterização
geracoes(5).
populacao(4).
prob_cruzamento(0.5).
prob_mutacao(0.5).  
:- dynamic(getSize/1). 

gera:-gera_populacao(P), 
	P=[L|_],
	length(L,N),
	retractall(getSize(N)), 
	asserta(getSize(N)),
	avalia_populacao(P,PA),
	ordena_populacao(PA,PORD),
	geracoes(NG),  
	gera_geracao(NG,PORD),!.
 
	 
%Lista N Populações que em cada população tem 1 caminho
gera_populacao(L):-populacao(N),
				gera_populacao(N,L)
				%write(L),nl
				.
gera_populacao(0,[]):-!. 
gera_populacao(N,L):- 
					N1 is N-1,
					gera_populacao(N1,NL),
					findall(C,city(C,_,_),LC),
					random_permutation(LC,LR),
					append([LR],NL,L).
 
				 
%Lista N Gerações em que serão feito cruzamentos com a populacao passada por parametro 
gera_geracao(0,P):-!,write('Geracao '), write(0), write(':'), nl, write(P), nl.
gera_geracao(G,P):-write('Geracao '), write(G), write(':'), nl, write(P), nl,
					cruzamento(P,NPop1),
					mutacao(NPop1,NPop),
					avalia_populacao(NPop,NPopAv),
					%write('mutacao'),nl,write(NPop1),nl,write(NPop),nl,  
					ordena_populacao(NPopAv,NPopOrd),
					G1 is G-1,
					gera_geracao(G1,NPopOrd). 


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	prob_cruzamento(Pcruz),	random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)), 
	cruzamento(Resto,Resto1).
   
gerar_pontos_cruzamento(P1,P2):-gerar_pontos_cruzamento1(P1,P2). 
gerar_pontos_cruzamento1(P1,P2):-getSize(N),
							NTemp is N+1,
							random(1,NTemp,P11),
							random(1,NTemp,P21),
							P11\==P21,!,
							((P11<P21,!,P1=P11,P2=P21);P1=P21,P2=P11).
gerar_pontos_cruzamento1(P1,P2):-gerar_pontos_cruzamento1(P1,P2).
 
cruzar(Ind1,Ind2,P1,P2,NInd11):-sublista(Ind1,P1,P2,Sub1),
							getSize(NumT),
							R is NumT-P2,
							rotate_right(Ind2,R,Ind21),
							elimina(Ind21,Sub1,Sub2),
							P3 is P2 + 1,
							insere(Sub2,Sub1,P3,NInd1),
							eliminah(NInd1,NInd11).
avalia_populacao([],[]).
avalia_populacao([I|R],[I*D|R1]):-calDist(I,D), %Fitness 
								avalia_populacao(R,R1).
/*------------ORDENACAO DA LISTA--------------------------------*/
ordena_populacao(PA,PORD):-
	bsort(PA,PORD). 
bsort([X],[X]):-!.
bsort([X|Xs],Ys):-bsort(Xs,Zs),
				btroca([X|Zs],Ys). 
btroca([X],[X]):-!. 
btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-VX>VY,!,
								btroca([X*VX|L1],L2). 
btroca([X|L1],[X|L2]):-btroca(L1,L2).
/*------------SUB LISTA-----------------------------------------*/
sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L). 
sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L). 
sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H). 
sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2). 
sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2). 
preencheh([],[]). 
preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2). 
/*---------------------------------------------------------*/	 
rotate_right(L,K,L1):-
	getSize(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!. 
rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).

/*-------------INSERCAO | ELIMINACAO---------------------*/
insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	getSize(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).
 
insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

elimina([],_,[]):-!. 
elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).
 
eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2). 
/*----------mutation operator--------------*/
mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd). 
/*------------------------*/

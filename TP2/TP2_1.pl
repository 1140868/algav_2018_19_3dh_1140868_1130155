estimativa(Nodo1,Nodo2,Estimativa):-
    city(Nodo1,X1,Y1),
    city(Nodo2,X2,Y2), 
    Estimativa is sqrt((X1-X2)^2+(Y1-Y2)^2).

aStar(Orig,Dest,Cam,Custo):- 
    aStar2(Dest,[(_,0,[Orig])],Cam,Custo).

aStar2(Dest,[(_,Custo,[Dest|T])|_],Cam,Custo):-
    reverse([Dest|T],Cam).
aStar2(Dest,[(_,Ca,LA)|Outros],Cam,Custo):-
    LA=[Act|_],
    findall((CEX,CaX,[X|LA]),
    (Dest\==Act,dist_cities(Act,X,CustoX),\+ member(X,LA),
    CaX is CustoX + Ca, estimativa(X,Dest,EstX),
    CEX is CaX +EstX),Novos),
    append(Outros,Novos,Todos),
    sort(Todos,TodosOrd),
    aStar2(Dest,TodosOrd,Cam,Custo).

/*Ex1*/

/*Vai buscar as cidades existentes na BC*/
cidades(L):- 
    findall(C, city(C,_,_),L).


tsp1(Orig,Cam,Custo):-
    /*O destino é na mesma a origem*/
    aStar(Orig,_,Cam1,Custo1),
    length(Cam1,Tam1),
    cidades(L),
    length(L,Tam2),
    Tam1 == Tam2,
    last(Cam1,Last),
    dist_cities(Last,Orig,C),
    Custo is C+Custo1,
    append(Cam1,Orig,Cam), !.

/*Ex2*/

/*O numero maximo de cidades que o predicado anterior tem capacidade para resolver é 9.
Se adicionar a setima, começa a ficar lento e a partir da cidade 9 o SWI-PROLOG encerra
sem dar resultados.*/